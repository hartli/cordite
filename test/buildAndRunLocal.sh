#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
pushd ../cordapps
./gradlew deployNodes buildNode -PCI_COMMIT_REF_NAME=a -PCI_COMMIT_SHA=b -PCI_PIPELINE_ID=c

popd

pushd ../node
docker build -t cordite/cordite:local .

popd

IMAGE_TAG=cordite/cordite:local ./build_env.sh