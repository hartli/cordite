/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl

import io.bluebank.braid.core.async.getOrThrow
import io.cordite.dgl.corda.LedgerApi
import io.cordite.test.utils.BraidPortHelper
import io.cordite.test.utils.run
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import net.corda.core.identity.CordaX500Name
import net.corda.finance.contracts.asset.OnLedgerAsset
import net.corda.testing.node.MockNetwork
import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertTrue

// TODO: https://gitlab.com/cordite/cordite/issues/194
@Ignore
@RunWith(VertxUnitRunner::class)
class FaultIsolation2Test {
  lateinit var network: MockNetwork
  private lateinit var ledger: LedgerApi
  private val nodeNameA = CordaX500Name("nodeA", "London", "GB")
  private val braidPortHelper = BraidPortHelper()
  private lateinit var testNodeA: TestNode

  private lateinit var defaultNotaryName: CordaX500Name

  @Before
  fun before() {
    braidPortHelper.setSystemPropertiesFor(nodeNameA)
    network = MockNetwork(listOf(this::class.java.`package`.name, OnLedgerAsset::class.java.`package`.name))
    val nodeA = network.createPartyNode(nodeNameA)
    network.runNetwork()
    defaultNotaryName = network.defaultNotaryIdentity.name
    testNodeA = TestNode(nodeA, braidPortHelper)
    ledger = testNodeA.ledgerService
  }

  @After
  fun after() {
    testNodeA.shutdown()
  }

  @Test
  fun step1(context: TestContext) {
    context.async()
    val future = ledger.listTokenTypes()
    val result1 = future.getOrThrow()
    println(result1)
  }

  @Test
  fun step2() {
    val result2 = ledger.wellKnownTagCategories()
    println(result2)
  }

  @Test
  fun step1and2() {
    val token = run(network) { ledger.createTokenType("XCD", 2, defaultNotaryName) }
    val tokens = run(network) { ledger.listTokenTypes() }
    assertTrue(tokens.contains(token))

    println(tokens)
    val result2 = ledger.wellKnownTagCategories()
    println(result2)
  }
}