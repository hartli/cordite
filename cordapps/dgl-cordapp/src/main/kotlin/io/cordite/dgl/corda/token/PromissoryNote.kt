/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.token

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.corda.account.AccountAddress
import net.corda.core.contracts.*
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.AbstractParty
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.transactions.SignedTransaction
import net.corda.finance.contracts.asset.Cash
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table


// TODO: WIP to adapt to new Token model - https://gitlab.com/cordite/cordite/issues/290
data class PromissoryNote(
    override val amount: Amount<Issued<TokenType.Descriptor>>,
    override val owner: AbstractParty,
    val ownerAccountId: AccountAddress,
    val onDemand: Boolean = true,
    val maturityDate: Date = Date()
) : FungibleAsset<TokenType.Descriptor>, QueryableState {
  override val participants: List<AbstractParty> = listOf(owner)
  override val exitKeys = setOf(owner.owningKey, amount.token.issuer.party.owningKey)

  override fun toString() = "\uD83D\uDCB8 ${amount.token.issuer.party.nameOrNull()?.organisation} promises to pay ${amount.quantity} ${amount.token.product.symbol} to ${owner.nameOrNull()?.organisation}"

  override fun withNewOwner(newOwner: AbstractParty) = CommandAndState(Cash.Commands.Move(), copy(owner = newOwner))

  override fun withNewOwnerAndAmount(
      newAmount: Amount<Issued<TokenType.Descriptor>>,
      newOwner: AbstractParty
  ): FungibleAsset<TokenType.Descriptor> {
    return copy(amount = amount.copy(newAmount.quantity), owner = newOwner)
  }

  override fun generateMappedObject(schema: MappedSchema): PersistentState {
    return when (schema) {
      is PromissoryNoteSchemaV1 -> PromissoryNoteSchemaV1.PromissoryNoteSchema(amount.token.product.symbol, amount.token.issuer.party, ownerAccountId.toString(), amount.quantity)
      else -> throw RuntimeException("Unknown schema type: ${schema.javaClass.canonicalName}")
    }
  }

  override fun supportedSchemas() = listOf(PromissoryNoteSchemaV1)
}


interface PromissoryNoteCommand : CommandData {
  data class Move(override val contract: Class<out Contract>? = null) : MoveCommand, PromissoryNoteCommand
  class Issue : TypeOnlyCommandData(), PromissoryNoteCommand
  data class Settle(val settlementInstruction: SettlementInstruction): PromissoryNoteCommand
}

// TODO: the following obviously needs to be written properly! https://gitlab.com/cordite/cordite/issues/291
sealed class SettlementInstruction() {
  data class UKSettlement(val sortCode: String, val account: String, val reference: String) : SettlementInstruction()
  data class InternationalSettlement(val bic: String, val iban: String, val reference: String): SettlementInstruction()
}

class PromissoryNoteContract : Contract {
  override fun verify(tx: LedgerTransaction) {
    val command = tx.commands.requireSingleCommand<PromissoryNoteCommand>()
    val groups = tx.groupStates(PromissoryNote::class.java) { it.amount.token }
    for ((inputs, outputs, _) in groups) {
      when (command.value) {
        is PromissoryNoteCommand.Issue -> {
          requireThat { "there are no inputs" using (inputs.isEmpty()) }
          requireThat { "there are one or more outputs" using (outputs.isNotEmpty()) }
        }
        is PromissoryNoteCommand.Move -> {
          requireThat {
            "there are one or more inputs " using (inputs.isNotEmpty())
          }
          requireThat {
            "total input amount equals total output amount" using (inputs.map { it.amount.quantity }.sum() == outputs.map { it.amount.quantity}.sum())
          }
        }
        is PromissoryNoteCommand.Settle -> {
          requireThat { "there are one or more inputs" using (inputs.isNotEmpty())}
          requireThat { "there are zero outputs" using (outputs.isEmpty())}
        }
      }
    }

  }
}

object PromissoryNoteSchema

object PromissoryNoteSchemaV1 : MappedSchema(PromissoryNoteSchema::class.java, 1, setOf(PromissoryNoteSchemaV1.PromissoryNoteSchema::class.java)) {
  @Entity
  @Table(name = "CORDITE_PROMISSORY_NOTE")
  class PromissoryNoteSchema(
      @Column(name = "symbol")
      val tokenSymbol: String,
      @Column(name = "issuer")
      val issuer: AbstractParty,
      @Column(name = "account")
      val accountId: String,
      @Column(name = "amount")
      val amount: Long
  ) : PersistentState()
}


class IssuePromissaryNote(private val amount: Amount<TokenType>) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {

    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }
}
