/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.utils

import io.vertx.core.Future
import net.corda.core.concurrent.CordaFuture
import net.corda.core.utilities.getOrThrow


fun <T, R> Future<T>.flatMap(fn: (T) -> Future<R>) : Future<R> {
  try {
    val result = Future.future<R>()
    this.setHandler {
      if (it.failed()) {
        result.fail(it.cause())
      } else {
        try {
          fn(it.result()).setHandler {
            result.completer().handle(it)
          }
        } catch (err: Throwable) {
          result.fail(err)
        }
      }
    }
    return result
  } catch (err: Throwable) {
    return Future.failedFuture(err)
  }
}

fun <T> CordaFuture<T>.toVertxFuture() : Future<T> {
  val result = Future.future<T>()
  this.then { f ->
    try {
      result.complete(f.getOrThrow())
    } catch (err: Throwable) {
      result.fail(err)
    }
  }
  return result
}