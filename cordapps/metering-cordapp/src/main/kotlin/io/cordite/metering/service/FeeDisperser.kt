/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import io.cordite.metering.contract.MeteringInvoiceState
import io.cordite.metering.contract.MeteringState
import io.cordite.metering.daostate.MeteringModelData
import io.cordite.metering.flow.DisperseMeteringInvoiceFundsFlow
import io.cordite.metering.flow.MeteringInvoiceFlowCommands
import io.cordite.metering.schema.MeteringInvoiceSchemaV1
import net.corda.core.contracts.StateAndRef
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder
import net.corda.core.utilities.getOrThrow
import net.corda.core.utilities.loggerFor

class FeeDisperser(val serviceHub : AppServiceHub, feeDispersalServiceConfig: FeeDispersalServiceConfig){

  private val meteringDaoStateLoader = MeteringDaoStateLoader(serviceHub, feeDispersalServiceConfig.daoName)
  private val log = loggerFor<AbstractMeterer>()
  private val myIdentity = serviceHub.myInfo.legalIdentities.first()
  private lateinit var meteringModelData: MeteringModelData

  init {
    log.info("Fee Disperser Created")
  }

  fun DisperseFeesForPaidInvoices(){

    if(!weAreReadyToDisperseFees()){
      return
    }

    val paidInvoices = getPaidMeteringInvoices()

    paidInvoices.forEach {

      val meteringInvoice = it.state.data

      val disperseeMeteringInvoiceRequest = MeteringInvoiceFlowCommands.DisperseFundsForMeteringInvoiceRequest(meteringInvoice.meteringInvoiceProperties.meteredTransactionId)
      val disperseMeteringInvoiceFlow = DisperseMeteringInvoiceFundsFlow.MeteringInvoiceFundDisperser(disperseFundsForMeteringInvoiceRequest = disperseeMeteringInvoiceRequest,
        meteringModelData = meteringModelData)
       val disperseMeteringInvoiceFlowFuture = serviceHub.startFlow(disperseMeteringInvoiceFlow).returnValue

      log.debug("About to disperse PAID Metering Invoice ${meteringInvoice.meteringInvoiceProperties.meteredTransactionId}")
      val result = disperseMeteringInvoiceFlowFuture.getOrThrow()

      log.debug("Successfully Dispersed Fees txid ${result.coreTransaction.id}")
    }
  }

  private fun weAreReadyToDisperseFees(): Boolean {

    //TODO - note: this loads the dao state each time, but we could change it to listener to make this more efficient https://gitlab.com/cordite/cordite/issues/196
    val loadedMeteringModelData = meteringDaoStateLoader.getMeteringDao()
    //Have we got a Dao?
    if (loadedMeteringModelData == null) {
      return false
    }

    log.debug("We have a metering DaoState")
    meteringModelData = loadedMeteringModelData

    return true
  }

  private fun getPaidMeteringInvoices(): List<StateAndRef<MeteringInvoiceState>> {
    val result = builder {
      val expression = MeteringInvoiceSchemaV1.PersistentMeteringInvoice::meteringState.equal(MeteringState.PAID.name)
      val customCriteria = QueryCriteria.VaultCustomQueryCriteria(expression)
      val generalCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.ALL)
      val fullCriteria = generalCriteria.and(customCriteria)
      val states = serviceHub.vaultService.queryBy<MeteringInvoiceState>(fullCriteria).states
      states
    }
    return result
  }
}


