/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import org.junit.Test

class MeteringServiceConfigTests {

  @Test
  fun `I can load the config with all the parameters populated with default file`(){
    val meteringServiceConfig = MeteringServiceConfig.loadConfig()
    assert(meteringServiceConfig.daoPartyName=="Cordite Committee")
    assert(meteringServiceConfig.meteringRefreshInterval==2000L)
    assert(meteringServiceConfig.meteringServiceType==MeteringServiceType.SingleNodeMeterer)
    assert(meteringServiceConfig.meteringNotaryAccountId=="metering-account-acc1")
    assert(meteringServiceConfig.daoName=="Cordite Committee")
    assert(meteringServiceConfig.meteringPartyName=="Cordite Metering Notary")
  }

  @Test
  fun `I can load the config with all the parameters populated with a file of my choice`(){
    val meteringServiceConfig = MeteringServiceConfig.loadConfig("metering-config-file-of-my-choice.json")
    assert(meteringServiceConfig.daoPartyName=="daodao")
    assert(meteringServiceConfig.meteringRefreshInterval==3000L)
    assert(meteringServiceConfig.meteringServiceType==MeteringServiceType.BftSmartMeterer)
    assert(meteringServiceConfig.meteringNotaryAccountId=="metering-account-acc2")
    assert(meteringServiceConfig.daoName=="SomeOtherDao")
    assert(meteringServiceConfig.meteringPartyName=="MeteringNotary")
  }

  @Test
  fun `I will resort to defaults if I cant find a file`(){
    val meteringServiceConfig = MeteringServiceConfig.loadConfig("you-will-never-find-this-metering-config-file-of-my-choice.json")
    assert(meteringServiceConfig.daoPartyName=="")
    assert(meteringServiceConfig.meteringRefreshInterval==2000L)
    assert(meteringServiceConfig.meteringServiceType==MeteringServiceType.SingleNodeMeterer)
    assert(meteringServiceConfig.meteringNotaryAccountId=="")
    assert(meteringServiceConfig.daoName=="")
    assert(meteringServiceConfig.meteringPartyName=="")

  }
}