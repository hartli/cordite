/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


const Proxy = require('braid-client').Proxy;

const emea = new Proxy({url: 'https://emea-test.cordite.foundation:8080/api/'}, onOpen, onClose, onError, {strictSSL: false})

let saltedTokenName = 'TOK-'+new Date().getTime()
let saltedAccountName = 'Acc-'+new Date().getTime()
let notary = "OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB"

function onOpen() {
    console.log("connected to the emea test cordite node")

    emea.ledger.createTokenType(saltedTokenName, 2, notary).then( a => {
        console.log("Token with name " + saltedTokenName + " created")
        return emea.ledger.createAccount(saltedAccountName, notary)
    }).then( b => {
        console.log("Account with name " + saltedAccountName + " created")
        return emea.ledger.issueToken(saltedAccountName, 100, saltedTokenName, "First issuance", notary)
    }).then( c => {
        console.log("Tokens of type " + saltedTokenName + " issued to " + saltedAccountName)
        return emea.ledger.balanceForAccount(saltedAccountName)
    }).then( d => {
        bal = (d[0].quantity * d[0].displayTokenSize) + " " + d[0].token.symbol
        // bal = val 
        console.log("Balance for " + saltedAccountName + ": " + bal)
    }).catch(error => {
        console.error(error)
    })
}

function onClose() {
    console.log("closed")
}

function onError(err) {
    console.error(err)
}
