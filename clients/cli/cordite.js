#!/usr/bin/env node
/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
const os = require('os');
const repl = require('repl');
const fs = require('fs');
const Proxy = require('braid-client').Proxy;
const deepromisify = require('./deepromise-repl');
const historyPath = os.homedir() + '/.cordite_repl_history'
const corditeBanner = `  _____            ___ __     
 / ___/__  _______/ (_) /____ 
/ /__/ _ \\/ __/ _  / / __/ -_)
\\___/\\___/_/  \\_,_/_/\\__/\\__/`

var url = 'https://localhost:8081/api/';
var corda = null;
const notaries = {};
const globalSymbolNames = [];

function printBanner() {
  console.log('');
  console.log(corditeBanner);
}

function printHelp() {
  console.log('cordite [protocol://host:port]');
  console.log('example:');
  console.log('    cordite https://localhost:8081');
}

function run() {
  var args = process.argv;
  if (args.length >= 3) {
    const p = args[2];
    if (p === '--help' || p === '-h') {
      printHelp();
      process.exit();
    } else {
      url = p + '/api/';
    }
  }
  printBanner();
  console.log("connecting to", url);
  corda = new Proxy({
    url: url
  }, onOpen, onClose, onError, { strictSSL: false });
}

function onOpen() {
  printMyInfo()
    .then(() => {
      globalSymbolNames.push("corda", "notaries");
      Object.getOwnPropertyNames(corda).forEach(name => {
        globalSymbolNames.push(name);
      });
      console.log('available objects:', globalSymbolNames.join(', '));

      replServer = repl.start({
        prompt: 'cordite > ',
      });

      replServer.context.corda = corda;
      replServer.context.notaries = notaries;
      Object.getOwnPropertyNames(corda).forEach(name => {
        Object.defineProperty(replServer.context, name, { value: corda[name], writable: false });
        globalSymbolNames.push(name);
      });

      if (fs.existsSync(historyPath)) {
        fs.statSync(historyPath);
        fs.readFileSync(historyPath)
          .toString()
          .split('\n')
          .reverse()
          .filter(line => line.trim())
          .map(line => replServer.history.push(line));
      }

      replServer.on('exit', () => {
        fs.appendFileSync(historyPath, replServer.lines.join('\n'));
        process.exit();
      });

      deepromisify(replServer);
      return corda.network.notaryIdentities().then(n => {
        processNotaries(n);
      })
    })
    .catch(err => {
      console.error("failed during initialisation with error:", err);
      process.exit();
    });
}

function printMyInfo() {
  return corda.network.myNodeInfo()
    .then(ni => {
      console.log('');
      console.log('connected to node:', ni.legalIdentities[0].name)
      console.log('');
    });
}

function onError(e) {
  console.error('could not connect', e);
}

function onClose() {
  console.log('closed');
}

function processNotaries(unparsedNotaries) {
  const parsed = unparsedNotaries.map(n => {
    const name = n.name;
    const parsedName = name.split(',').map(i => i.trim()).map(i => i.split('='))
      .map(i => { 
        const o = {};
        o[i[0]] = i[1];
        return o;
      })
      .reduce((acc, current) => {
        return Object.assign(acc, current)
      }, {})
      notaries[decapitalize(parsedName.O).split(' ').join('')] = n
  });
}

function decapitalize(str) {
  if (str.length === 0) return str;
  return str.charAt(0).toLowerCase() + str.substr(1);
}

run();
